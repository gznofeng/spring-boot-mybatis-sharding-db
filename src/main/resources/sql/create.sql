CREATE TABLE `order_state` (
	`id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
	`order_sn` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '订单号' COLLATE 'utf8_general_ci',
	`release_state` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '释放状态，0：未释放，1：准备释放，2：已释放',
	`release_action_time` TIMESTAMP NOT NULL DEFAULT '1980-01-01 00:00:00' COMMENT '释放执行时间',
	`create_time` TIMESTAMP NOT NULL DEFAULT '1980-01-01 00:00:00' COMMENT '创建时间',
	`update_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最近更新时间',
	PRIMARY KEY (`id`) USING BTREE,
	UNIQUE INDEX `uniq_order_sn` (`order_sn`) USING BTREE,
	INDEX `release_status_release_action_time` (`release_state`, `release_action_time`) USING BTREE,
	INDEX `idx_create_time` (`create_time`) USING BTREE
)
COMMENT='订单状态表'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1
;
