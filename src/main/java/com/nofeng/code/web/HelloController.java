package com.nofeng.code.web;

import com.nofeng.code.entity.OrderState;
import com.nofeng.code.service.OrderStateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Locale;
import java.util.UUID;

@RestController
public class HelloController {

    @Autowired
    OrderStateService orderStateService;

    @RequestMapping("/hello")
    public String hello(Locale locale, Model model) {
        OrderState orderState = new OrderState();
        orderState.setOrderSn(UUID.randomUUID().toString());
        orderState.setReleaseState(Byte.valueOf("1"));
        int i = orderStateService.insert(orderState);

        OrderState orderState2 = new OrderState();
        orderState2.setOrderSn(UUID.randomUUID().toString());
        orderState2.setReleaseState(Byte.valueOf("1"));
        i += orderStateService.insert1(orderState2);

        OrderState orderState3 = new OrderState();
        orderState3.setOrderSn(UUID.randomUUID().toString());
        orderState3.setReleaseState(Byte.valueOf("1"));

        try {
            orderStateService.insertAll(orderState3);
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return "Hello World" + i;
    }

}