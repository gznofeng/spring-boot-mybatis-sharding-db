package com.nofeng.code.dao;

import com.nofeng.code.entity.OrderState;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderStateDao {
    int deleteByPrimaryKey(Long id);

    int insert(OrderState record);

    int insertSelective(OrderState record);

    OrderState selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(OrderState record);

    int updateByPrimaryKey(OrderState record);
}