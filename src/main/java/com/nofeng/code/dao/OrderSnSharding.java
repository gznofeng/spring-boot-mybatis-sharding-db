package com.nofeng.code.dao;

import com.nofeng.code.plugin.sharedingdb.RepositoryShardingStrategy;
import org.springframework.stereotype.Component;

import java.util.zip.CRC32;

@Component
public class OrderSnSharding implements RepositoryShardingStrategy {
    @Override
    public String sharding(Object obj) {
        String orderSn = (String) obj;
        CRC32 crc32 = new CRC32();
        crc32.update(orderSn.getBytes());
        long value = Math.abs(crc32.getValue());
        return "db_" + (value % 8);
    }
}
