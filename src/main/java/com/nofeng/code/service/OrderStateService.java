package com.nofeng.code.service;

import com.nofeng.code.dao.OrderStateDao;
import com.nofeng.code.entity.OrderState;
import com.nofeng.code.plugin.sharedingdb.annocation.RepositorySharding;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class OrderStateService {

    @Autowired
    OrderStateDao orderStateDao;

    /**
     * 以订单号分库
     *
     * @param record
     * @return
     */
    @RepositorySharding(strategy = "orderSnSharding", key = "#record.orderSn", name = "")
    public int insert(OrderState record) {
        return orderStateDao.insert(record);
    }

    /**
     * 直接指定库
     *
     * @param record
     * @return
     */
    @RepositorySharding(strategy = "", key = "#record.orderSn", name = "db_1")
    public int insert1(OrderState record) {
        return orderStateDao.insert(record);
    }

    @Transactional(rollbackFor = Throwable.class)
    @RepositorySharding(strategy = "", key = "#record.orderSn", name = "db_4")
    public int insertAll(OrderState record) {
        orderStateDao.insert(record);
        throw new NullPointerException("error");
    }
}
