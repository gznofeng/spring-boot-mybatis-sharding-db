package com.nofeng.code.plugin.sharedingdb.model;

import lombok.Data;

import java.util.List;

@Data
public class MatrixDatasourceModel {
    private String matrixName;
    private String state;
    private String type;
    private List<GroupModel> groups;
    private List<RuleModel> rules;
    private List<ShardStrategyModel> strategies;

}
