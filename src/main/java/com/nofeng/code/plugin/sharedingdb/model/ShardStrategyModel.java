package com.nofeng.code.plugin.sharedingdb.model;

import lombok.Data;

import java.util.List;

@Data
public class ShardStrategyModel {
    private List<RepoShardStrategyModel> repoShardStrategies;
    private List<TableShardStrategyModel> tableShardStrategies;
}
