package com.nofeng.code.plugin.sharedingdb.model;

import lombok.Data;

@Data
public class TableShardStrategyModel {
    private String strategyName;
    private String strategy;
    private String tableSuffix;
}
