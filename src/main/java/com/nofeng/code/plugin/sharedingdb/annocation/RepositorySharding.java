package com.nofeng.code.plugin.sharedingdb.annocation;

import java.lang.annotation.*;


@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface RepositorySharding {
    /**
     * 分库策略名
     */
    String strategy();

    /**
     * 分库参数
     */
    String key();

    /**
     * 指定库
     */
    String name();
}
