package com.nofeng.code.plugin.sharedingdb.model;

import lombok.Data;

import java.util.List;

@Data
public class GroupModel {
    private String groupName;
    private String state;
    private String loadBalance;
    private List<AtomModel> atoms;
}
