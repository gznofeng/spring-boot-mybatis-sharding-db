package com.nofeng.code.plugin.sharedingdb.model;

import lombok.Data;

@Data
public class RepoShardStrategyModel {
    private String strategyName;
    private String strategy;
    private String groupIndex;
}
