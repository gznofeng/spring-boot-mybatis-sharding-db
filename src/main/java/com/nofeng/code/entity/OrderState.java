package com.nofeng.code.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * @author
 */
public class OrderState implements Serializable {
    /**
     * 主键
     */
    private Long id;

    /**
     * 订单号
     */
    private String orderSn;

    /**
     * 释放状态，0：未释放，1：准备释放，2：已释放
     */
    private Byte releaseState;

    /**
     * 释放执行时间
     */
    private Date releaseActionTime;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 最近更新时间
     */
    private Date updateTime;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrderSn() {
        return orderSn;
    }

    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn;
    }

    public Byte getReleaseState() {
        return releaseState;
    }

    public void setReleaseState(Byte releaseState) {
        this.releaseState = releaseState;
    }

    public Date getReleaseActionTime() {
        return releaseActionTime;
    }

    public void setReleaseActionTime(Date releaseActionTime) {
        this.releaseActionTime = releaseActionTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        OrderState other = (OrderState) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
                && (this.getOrderSn() == null ? other.getOrderSn() == null : this.getOrderSn().equals(other.getOrderSn()))
                && (this.getReleaseState() == null ? other.getReleaseState() == null : this.getReleaseState().equals(other.getReleaseState()))
                && (this.getReleaseActionTime() == null ? other.getReleaseActionTime() == null : this.getReleaseActionTime().equals(other.getReleaseActionTime()))
                && (this.getCreateTime() == null ? other.getCreateTime() == null : this.getCreateTime().equals(other.getCreateTime()))
                && (this.getUpdateTime() == null ? other.getUpdateTime() == null : this.getUpdateTime().equals(other.getUpdateTime()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getOrderSn() == null) ? 0 : getOrderSn().hashCode());
        result = prime * result + ((getReleaseState() == null) ? 0 : getReleaseState().hashCode());
        result = prime * result + ((getReleaseActionTime() == null) ? 0 : getReleaseActionTime().hashCode());
        result = prime * result + ((getCreateTime() == null) ? 0 : getCreateTime().hashCode());
        result = prime * result + ((getUpdateTime() == null) ? 0 : getUpdateTime().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", orderSn=").append(orderSn);
        sb.append(", releaseState=").append(releaseState);
        sb.append(", releaseActionTime=").append(releaseActionTime);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}