# spring-boot-mybatis-sharding-db

#### 介绍
springboot应用中使用mybatis进行分库操作的demo

#### 使用说明

1.application.properties 中添加对应的数据库配置,db.config.datasource.db_0 代表分库0，db.config.datasource.db_1 代表分库1。db.config.datasource为固定前缀，db_x为分库的自定义名称，在后续的分库策略中会使用到。
2.定义分库策略，添加分为策略的实现类，实现RepositoryShardingStrategy接口,具体可参考OrderSnSharding

#### 如何配置
![Image text](https://gitee.com/gznofeng/spring-boot-mybatis-sharding-db/raw/master/readme/howtoconfig.png)

#### 如何配置分库策略
![Image text](https://gitee.com/gznofeng/spring-boot-mybatis-sharding-db/raw/master/readme/sharding.png)

#### 如何使用
![Image text](https://gitee.com/gznofeng/spring-boot-mybatis-sharding-db/raw/master/readme/howtouse.png)
